# SOME DESCRIPTIVE TITLE.
# Copyright (C) 1996, 1997, 1998 Ian Jackson, Christian Schwarz, 1998-2017,
# The Debian Policy Mailing List
# This file is distributed under the same license as the Debian Policy
# Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2018.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Debian Policy Manual 4.1.6.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-08-03 09:57+0800\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.4.0\n"

#: ../../index.rst:3
msgid "Debian Policy Manual"
msgstr ""

#: ../../index.rst:5
msgid ""
"This manual describes the policy requirements for the Debian "
"distribution. This includes the structure and contents of the Debian "
"archive and several design issues of the operating system, as well as "
"technical requirements that each package must satisfy to be included in "
"the distribution."
msgstr ""

#: ../../index.rst:10
msgid "This is Debian Policy version 4.2.0.0, released on 2018-08-02."
msgstr ""

#: ../../index.rst:29
msgid "Appendices"
msgstr ""

#~ msgid "This is Debian Policy version 4.1.6.0, released on 2018-07-11."
#~ msgstr ""

